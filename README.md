# Smart Avocat
<a href="https://ibb.co/nMGB3Zz"><img src="https://i.ibb.co/s9fC5ky/logo2.png" alt="logo2" border="0"></a>
<h1>Site Web : </h1>
<strong>1-</strong> Lancer la commande : <code><strong>npm install</strong></code> <br>
<strong>2-</strong> Lancer  : <code><strong>ng serve</strong></code> <br>
<h1>Application Flutter :</h1>
<strong>1-</strong> Installer l'apk se trouver dans le répértoire   :<code><strong>Executable Flutter</strong></code> <br>
<h1>WebServices :</h1>
<strong>1-</strong> Lancer la commande:<code><strong>npm install</strong></code> <br>
<strong>2-</strong> Lancer la commande:<code><strong>npm start</strong></code> <br>
<h1>Docker :</h1>
<strong>1-</strong> Créer un fichier vide nommé: <code><strong>docker-compose.yml</strong></code> <br>
<h2>Contenu:</h2>
<strong>2-</strong> Ajouter le code suivant dans ce fichier :
<pre>
<code>
version: "3"
services:
  angular:
    image: tarekbenyahia/smartavocatangulariteamz
    ports:
      - "4200:80"
  back:
    image: tarekbenyahia/backendsmartavocatwebserv
    environment:
      - NODE_ENV=production
    ports:
      - "3000:3000"
    volumes:
      - D:\FlutterProjects\smart_avocat_ITEAMZ\smart-avocat\Smart_avocat_Backend\Uploads:/src/app/Uploads/
</code>
</pre>
<h2>Execution</h2>
<strong>3-</strong> Executer ce conteneur avec : <code><strong> docker-compose up -d --build</strong></code>
<h2>Test:</h2>
<strong>4-</strong> Tester votre conteneur avec : <code> http://ADRESSEIP:3000/api/avocat/getAll</code>
<h2>Fermeture(Shutdown):</h2>
<strong>5-</strong> Fermer Votre conteneur avec : <code>docker-compose down</code>
Angular Website
Flutter Application
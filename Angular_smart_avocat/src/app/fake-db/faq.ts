import { environment } from "environments/environment.hmr";

export class FaqFakeDb
{
    public static data = [
        {
            'question': 'Questions concernant le droit civil et familial ',
            'answer': 'divorce & infidélité, garde des enfants, succession & patrimoine, pension alimentaire, changement de nom/prénom, successions, reconnaissance de paternité, vie privée, procédure civile, contrat, responsabilité, mariage, test ADN'
        },
        {
            'question': 'Questions concernant le droit pénal',
            'answer': ' Alcool & drogue, Abus de confiance, droit pénal routier, casier judiciaire, viol, vols, atteinte aux personnes, délit, cour d’assise, vie privée, droit pénal des affaires, juge des libertés et de la détention, juge d’instruction, droit pénal spécial '
        },
        {
            'question': 'Questions concernant le droit de limmobilier ',
            'answer': 'locataires, locations, construction, trouble du voisinage, travaux, urbanisme, malfaçons, achat/vente, baux commerciaux, copropriétaire, baux, droit de passage, titre de propriété, servitude, dégâts, préavis, permis de construire, assurance, entretien, rénovation, usufruit, charges, Saisies immobilières'
        },
        {
            'question': 'Questions concernant le droit de travail',
            'answer': 'Contrat de travail, Rupture de contrat, Licenciement, Licenciement économique, Maladies & accidents, Harcèlement, Licenciement pour faute, Démission forcée, Démission, Salaire, Licenciement négocié, Préavis, Abus de pouvoir, Retraite, Chômage, Temps de travail, Embauche'
        },
        {
            'question': 'Questions concernant le droit des entreprises',
            'answer': 'redressement judiciaire, liquidation, droit des sociétés'
        },
        {
            'question': 'Question concernant le droit commercial',
            'answer': 'bail commercial, contrat, fonds de commerce, contrats avec un particulier, concurrence déloyale/illicite, contrats entre professionnels, paiement'
        },
        {
            'question': 'Question concernant le droit des assurances',
            'answer': 'accident de la route, responsabilité civile, CPAM, délai de prescription, résiliation, sécurité sociale, Assurance automobile, assurance habitation, assurance vie'
        },
        {
            'question': 'Question concernant le droit bancaire',
            'answer': 'prêt, obligation dinformation, droit du crédit, devoir de conseil, recouvrement, comptes en banques, secret bancaire, Financement'
        },
        {
            'question': 'Question concernant le droit de lenvironment',
            'answer': 'Chasse et pêche, Gestion de leau, protection de la nature, Installations classées, Enquêtes publiques, Information du citoyen'
        },
        
        
       
    ];
}

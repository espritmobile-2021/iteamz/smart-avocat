import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
const AUTH_API = 'http://localhost:3000/api/admin/';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class AdminService {

  constructor(private http: HttpClient,
  ) { }

  getprofile(): Observable<any> {
    return this.http.get(AUTH_API + 'groupby');
  }

  dom(): Observable<any> {
    return this.http.get(AUTH_API + 'domaine');
  }
  cl(): Observable<any> {
    return this.http.get(AUTH_API + 'groupbycl');
  }
  city(): Observable<any> {
    return this.http.get(AUTH_API + 'city');
  }
}

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["common"], {
  /***/
  "./src/app/_services/rendezVous.service.ts":
  /*!*************************************************!*\
    !*** ./src/app/_services/rendezVous.service.ts ***!
    \*************************************************/

  /*! exports provided: rendezVousService */

  /***/
  function srcApp_servicesRendezVousServiceTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "rendezVousService", function () {
      return rendezVousService;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/common/http */
    "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
    /* harmony import */


    var _services_token_storage_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ../_services/token-storage.service */
    "./src/app/_services/token-storage.service.ts");

    var AUTH_API = 'http://localhost:3000/api/rendezvous/';
    var httpOptions = {
      headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({
        'Content-Type': 'application/json'
      })
    }; // tslint:disable-next-line:class-name

    var rendezVousService =
    /*#__PURE__*/
    function () {
      function rendezVousService(http, tokenStorageService) {
        _classCallCheck(this, rendezVousService);

        this.http = http;
        this.tokenStorageService = tokenStorageService;
        this.user = this.tokenStorageService.getUser();
      }

      _createClass(rendezVousService, [{
        key: "ajouterRdv",
        value: function ajouterRdv(form) {
          return this.http.post(AUTH_API + 'ajouterRdv', form);
        }
      }, {
        key: "getmyrdv",
        value: function getmyrdv() {
          console.log(this.user._id);
          return this.http.post(AUTH_API + 'getmyrdv', {
            id: this.user._id
          }, httpOptions);
        }
      }, {
        key: "getmyrdvc",
        value: function getmyrdvc() {
          console.log(this.user._id);
          return this.http.post(AUTH_API + 'getmyrdvc', {
            id: this.user._id
          }, httpOptions);
        }
      }, {
        key: "etarrdv",
        value: function etarrdv(idr) {
          console.log(this.user._id);
          return this.http.post(AUTH_API + 'etarrdv', {
            id: idr
          }, httpOptions);
        }
      }]);

      return rendezVousService;
    }();

    rendezVousService.ɵfac = function rendezVousService_Factory(t) {
      return new (t || rendezVousService)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_services_token_storage_service__WEBPACK_IMPORTED_MODULE_2__["TokenStorageService"]));
    };

    rendezVousService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({
      token: rendezVousService,
      factory: rendezVousService.ɵfac,
      providedIn: 'root'
    });
    /*@__PURE__*/

    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](rendezVousService, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"],
        args: [{
          providedIn: 'root'
        }]
      }], function () {
        return [{
          type: _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]
        }, {
          type: _services_token_storage_service__WEBPACK_IMPORTED_MODULE_2__["TokenStorageService"]
        }];
      }, null);
    })();
    /***/

  },

  /***/
  "./src/app/_services/suivie.service.ts":
  /*!*********************************************!*\
    !*** ./src/app/_services/suivie.service.ts ***!
    \*********************************************/

  /*! exports provided: SuivieService */

  /***/
  function srcApp_servicesSuivieServiceTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "SuivieService", function () {
      return SuivieService;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/common/http */
    "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
    /* harmony import */


    var _services_token_storage_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ../_services/token-storage.service */
    "./src/app/_services/token-storage.service.ts");

    var AUTH_API = 'http://localhost:3000/api/suivie/';
    var httpOptions = {
      headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({
        'Content-Type': 'application/json'
      })
    };

    var SuivieService =
    /*#__PURE__*/
    function () {
      function SuivieService(http, tokenStorageService) {
        _classCallCheck(this, SuivieService);

        this.http = http;
        this.tokenStorageService = tokenStorageService;
        this.user = this.tokenStorageService.getUser();
      }

      _createClass(SuivieService, [{
        key: "ajoutersuivie",
        value: function ajoutersuivie(id, id1, id2) {
          return this.http.post(AUTH_API + 'ajoutersuivie', {
            userid: id,
            avocatid: id1,
            rdvid: id2
          }, httpOptions);
        }
      }, {
        key: "rrdv",
        value: function rrdv(id) {
          return this.http.post(AUTH_API + 'rsuivie', {
            id: id
          }, httpOptions);
        }
      }, {
        key: "paysuivie",
        value: function paysuivie(id) {
          return this.http.post(AUTH_API + 'paysuivie', {
            id: id
          }, httpOptions);
        }
      }, {
        key: "getsuivie",
        value: function getsuivie(id) {
          return this.http.post(AUTH_API + 'getmysuivie', {
            id: id
          }, httpOptions);
        }
      }]);

      return SuivieService;
    }();

    SuivieService.ɵfac = function SuivieService_Factory(t) {
      return new (t || SuivieService)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_services_token_storage_service__WEBPACK_IMPORTED_MODULE_2__["TokenStorageService"]));
    };

    SuivieService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({
      token: SuivieService,
      factory: SuivieService.ɵfac,
      providedIn: 'root'
    });
    /*@__PURE__*/

    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](SuivieService, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"],
        args: [{
          providedIn: 'root'
        }]
      }], function () {
        return [{
          type: _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]
        }, {
          type: _services_token_storage_service__WEBPACK_IMPORTED_MODULE_2__["TokenStorageService"]
        }];
      }, null);
    })();
    /***/

  }
}]);
//# sourceMappingURL=common-es5.js.map
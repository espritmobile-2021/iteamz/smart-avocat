(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["common"],{

/***/ "./src/app/_services/rendezVous.service.ts":
/*!*************************************************!*\
  !*** ./src/app/_services/rendezVous.service.ts ***!
  \*************************************************/
/*! exports provided: rendezVousService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "rendezVousService", function() { return rendezVousService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
/* harmony import */ var _services_token_storage_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../_services/token-storage.service */ "./src/app/_services/token-storage.service.ts");





const AUTH_API = 'http://localhost:3000/api/rendezvous/';
const httpOptions = {
    headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({ 'Content-Type': 'application/json' })
};
// tslint:disable-next-line:class-name
class rendezVousService {
    constructor(http, tokenStorageService) {
        this.http = http;
        this.tokenStorageService = tokenStorageService;
        this.user = this.tokenStorageService.getUser();
    }
    ajouterRdv(form) {
        return this.http.post(AUTH_API + 'ajouterRdv', form);
    }
    getmyrdv() {
        console.log(this.user._id);
        return this.http.post(AUTH_API + 'getmyrdv', {
            id: this.user._id,
        }, httpOptions);
    }
    getmyrdvc() {
        console.log(this.user._id);
        return this.http.post(AUTH_API + 'getmyrdvc', {
            id: this.user._id,
        }, httpOptions);
    }
    etarrdv(idr) {
        console.log(this.user._id);
        return this.http.post(AUTH_API + 'etarrdv', {
            id: idr,
        }, httpOptions);
    }
}
rendezVousService.ɵfac = function rendezVousService_Factory(t) { return new (t || rendezVousService)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_services_token_storage_service__WEBPACK_IMPORTED_MODULE_2__["TokenStorageService"])); };
rendezVousService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({ token: rendezVousService, factory: rendezVousService.ɵfac, providedIn: 'root' });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](rendezVousService, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"],
        args: [{
                providedIn: 'root'
            }]
    }], function () { return [{ type: _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"] }, { type: _services_token_storage_service__WEBPACK_IMPORTED_MODULE_2__["TokenStorageService"] }]; }, null); })();


/***/ }),

/***/ "./src/app/_services/suivie.service.ts":
/*!*********************************************!*\
  !*** ./src/app/_services/suivie.service.ts ***!
  \*********************************************/
/*! exports provided: SuivieService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SuivieService", function() { return SuivieService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
/* harmony import */ var _services_token_storage_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../_services/token-storage.service */ "./src/app/_services/token-storage.service.ts");





const AUTH_API = 'http://localhost:3000/api/suivie/';
const httpOptions = {
    headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({ 'Content-Type': 'application/json' })
};
class SuivieService {
    constructor(http, tokenStorageService) {
        this.http = http;
        this.tokenStorageService = tokenStorageService;
        this.user = this.tokenStorageService.getUser();
    }
    ajoutersuivie(id, id1, id2) {
        return this.http.post(AUTH_API + 'ajoutersuivie', {
            userid: id,
            avocatid: id1,
            rdvid: id2
        }, httpOptions);
    }
    rrdv(id) {
        return this.http.post(AUTH_API + 'rsuivie', {
            id: id,
        }, httpOptions);
    }
    paysuivie(id) {
        return this.http.post(AUTH_API + 'paysuivie', {
            id: id,
        }, httpOptions);
    }
    getsuivie(id) {
        return this.http.post(AUTH_API + 'getmysuivie', {
            id: id,
        }, httpOptions);
    }
}
SuivieService.ɵfac = function SuivieService_Factory(t) { return new (t || SuivieService)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_services_token_storage_service__WEBPACK_IMPORTED_MODULE_2__["TokenStorageService"])); };
SuivieService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({ token: SuivieService, factory: SuivieService.ɵfac, providedIn: 'root' });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](SuivieService, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"],
        args: [{
                providedIn: 'root'
            }]
    }], function () { return [{ type: _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"] }, { type: _services_token_storage_service__WEBPACK_IMPORTED_MODULE_2__["TokenStorageService"] }]; }, null); })();


/***/ })

}]);
//# sourceMappingURL=common-es2015.js.map
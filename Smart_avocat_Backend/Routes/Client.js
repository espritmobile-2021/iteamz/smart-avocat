const router = require("express").Router();
const Client = require("../Models/Client");
const Avocat = require("../Models/Avocat");
const Chat = require("../Models/chat");
const {v4 : uuidv4} = require('uuid');
var multer  = require('multer');




const storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, './Uploads')
    },
    filename: function (req, file, cb) {
      cb(null, Date.now() + '.' + file.originalname.split('.')[file.originalname.split('.').length - 1])
    }
  })
  
  const upload = multer({ storage: storage })


  router.post('/profile' ,upload.single('file') , async (req,res) => {


     const client = new Client({   
         userid: req.body.userid,
         nom: req.body.nom,
         prenom: req.body.prenom,
         daten : req.body.daten,
         description: req.body.description,
         sexe: req.body.sexe,
         phone: req.body.phone,
         country: req.body.country,
         city: req.body.city,
         image: req.file.filename
     });

     const id=req.body.userid
  
     const profilExist = await Client.findOne({ userid : id})
    
     if (profilExist)
     {
         return res.status(400).send({ message: 'votre profile existe deja ' });
     }else{
  
        try{
          const savedClient = await client.save();
         res.send(savedClient);

            }catch(err){
         res.status(400).send(err);
            }
         }
        
});



router.post('/getprofile' , async (req,res) => {
  const id = req.body.userid;

  await Client.findOne({ userid : id} )
       .then(data => {
       if (!data)
            res.status(400).send({ message: "wlhhhh le "  });
          else 
          res.send(data);
             })
        .catch(err => {
          res
           .status(500)
            .send({ message: "not find " + id });
});
})


router.post('/getchatlist' , async (req,res) => {
  const id = req.body.id;

  await Client.findOne({ userid : id} )
       .then(data => {
       if (!data)
            res.status(400).send({ message: "wlhhhh le "  });
          else 
          res.send(data);
             })
        .catch(err => {
          res
           .status(500)
            .send({ message: "not find " + id });
});
})


  router.get('/file/:filename', (req, res) => {

    res.sendFile(process.cwd() + '/Uploads/' + req.params.filename)
  })

  router.post('/add' , async (req,res) => 
  {
  
    const avocat = req.body.avocat;
    const user = req.body.client;


const chatId = uuidv4()
const chat =new Chat ({
 idc    : chatId,
 Dialog: [
   {
     who : avocat.userid,
     message : "Vous pouvez désormais échanger et planifier un entretien avec lui !"
   }
 ]
})
chat.save()

    const chatav = {
      idc             : chatId,
      contactId      : user.userid,
      nom             : user.nom,
      prenom      : user.prenom,
      image             : user.image,
      lastMessageTime      : Date.now(),
      };

      const chatcl = {
        idc             : chatId,
        contactId      : avocat.userid,
        nom             : avocat.nom,
        prenom      : avocat.prenom,
        image             : avocat.image,
        lastMessageTime      : Date.now(),
        };


        Client.findOneAndUpdate(
          { userid : user.userid},
          { $push: { chat: chatcl}},
          {useFindAndModify : false}
        ).then( data =>
          {
          
          })

          Avocat.findOneAndUpdate(
            { userid : avocat.userid},
            { $push: { chat: chatav}},
            {useFindAndModify : false}
          ).then( data =>
            {
            
            })
 
    

 
 });

 router.get('/chat/:chatid' , async (req,res) => {

  Chat.findOne({ idc : req.params.chatid})
  .then(result => {

    res.send(result);
 });
 
})

router.post('/update' , async (req,res) => 
{
  
 Chat.findOneAndUpdate({ idc : req.body.chatId},
  { $push: { Dialog: req.body.Dialog}})
  .then(data =>{

  }
    )

})


module.exports = router;
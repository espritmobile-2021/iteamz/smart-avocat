const router = require("express").Router();
const Client = require("../Models/Client");
const Avocat = require("../Models/Avocat");


router.get('/groupby' , async (req,res) => {
    
  
     await Avocat.aggregate(
        [
            { "$group": {
                "_id": "$sexe",
                "count": { "$sum": 1 }
            }}
        ],
        function(err,docs) {
           if (err) console.log(err);
           res.send( docs );
        });
  


  
    
  });

  router.get('/groupbycl' , async (req,res) => {
    
  
    await Client.aggregate(
       [
           { "$group": {
               "_id": "$sexe",
               "count": { "$sum": 1 }
           }}
       ],
       function(err,docs) {
          if (err) console.log(err);
          res.send( docs );
       });

 });

 router.get('/domaine' , async (req,res) => {
    const chatcl = {
        name             : '',
        value      : null,
    
        };
  
    await Avocat.aggregate(
       [
           { "$group": {
               "_id": "$domaine",
              
               "value": { "$sum": 1 }
           }}
       ],
       function(err,docs) {
          if (err) console.log(err);
          res.send( docs );
          
       });

 });

 router.get('/city' , async (req,res) => {
    const chatcl = {
        name             : '',
        value      : null,
    
        };
  
    await Avocat.aggregate(
       [
           { "$group": {
               "_id": "$city",
              
               "value": { "$sum": 1 }
           }}
       ],
       function(err,docs) {
          if (err) console.log(err);
          res.send( docs );
          
       });

 });

module.exports = router;
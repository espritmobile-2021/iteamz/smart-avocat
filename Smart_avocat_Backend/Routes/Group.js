const router = require("express").Router();
const Group = require("../Models/Group");
const Avocat = require("../Models/Avocat");
const Chat = require("../Models/chat");
const {v4 : uuidv4} = require('uuid');
var multer  = require('multer');

const storage = multer.diskStorage({
   destination: function (req, file, cb) {
     cb(null, './uploads')
   },
   filename: function (req, file, cb) {
     cb(null, Date.now() + '.' + file.originalname.split('.')[file.originalname.split('.').length - 1])
   }
 })
 
 const upload = multer({ storage: storage })


 router.post('/ajouter'  ,upload.single('file'), async (req,res) => {
  const userss = {
    id : req.body.userid, 
}

const chatId = uuidv4()
   const group = new Group({   
       nom: req.body.nom,
       desc: req.body.desc,
       image: req.file.filename,
      type: req.body.type,
      userid: req.body.userid,
      idc: chatId,
      cat : req.body.cat,
      user : userss
   });

   const chat =new Chat ({
    idc    : chatId,
    Dialog: [
    {
        who : req.body.userid,
        message : "Vous pouvez désormais échanger avec ce vgroupe !"
    }
    ]
    })




   try{
     const savedgroup = await group.save()

        const gp = {
        idc: chatId,
        contactId: savedgroup._id,
        image: req.file.filename,
        nom: req.body.nom,
        lastMessageTime:  Date.now()   
        };
       
        await  Avocat.findOneAndUpdate(
          { userid : req.body.userid},
          { $push: { group: gp}}
        );
       await chat.save()
        
 
       await res.send(savedgroup);

     
   }catch(err){
res.status(400).send(err);
   }
});

router.post('/ajouterG'  ,upload.single('file'), async (req,res) => {
  const userss = {
    id : req.body.userid, 
}

   const group = new Group({   
       nom: req.body.nom,
       desc: req.body.desc,
       image: req.file.filename,
      type: req.body.type,
      userid: req.body.userid,
      cat : req.body.cat,
      user : userss
   });

   try{
     const savedgroup = await group.save();
     res.send(savedgroup);
   }catch(err){
res.status(400).send(err);
   }
})




router.get('/getgroups' , async (req,res) => {

    const group =  Group;
     await group.find( )
         .then(data => {
         if (!data)
              res.status(404).send({ message: "Pas de group "});
            else res.send(data);
               })
          .catch(err => {
            res
             .status(500)
              .send({ message: "not find " });
});
})


router.post('/mygroup'  , async (req,res) => {

Group.find({"userid" : req.body.myid})
.then(data => {
  if (!data)
       res.status(404).send({ message: "no data "  });
     else 
     res.send(data);
        })
   .catch(err => {
     res
      .status(500)
       .send({ message: "not find "});
});

})


router.post('/rejoindre'  , async (req,res) => {
  const gp = {
    idc: req.body.idc,
    contactId: req.body.id,
    image: req.body.image,
    nom: req.body.nom,
    lastMessageTime:  Date.now()   
    };


    const userss = {
      id : req.body.userid, 
    }
    await  Avocat.findOneAndUpdate(
      { userid : req.body.userid},
      { $push: { group: gp}}
    );
    await  Group.findOneAndUpdate(
      { _id : req.body.id,},
      { $push: { user: userss}}
    );
  res.send(gp)
  })

router.post('/group'  , async (req,res) => {

  Group.findOne({_id : req.body.myid})
  .then(data => {
    if (!data)
         res.status(404).send({ message: "no data "  });
       else 
       res.send(data);
          })
     .catch(err => {
       res
        .status(500)
         .send({ message: "not find "});
  });
  
  })


module.exports = router;
const mongoose = require("mongoose");

const chatScheme = new mongoose.Schema({
idc : {
type : String 
},
Dialog : [
    {
    who : String,
    message : String,
    time :  { type: Date, default: Date.now }
}
]
})

module.exports =mongoose.model('Chat',chatScheme);
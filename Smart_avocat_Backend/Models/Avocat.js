const mongoose = require("mongoose");

const AvocatSchema = new mongoose.Schema({
userid : {
    type : String
}   , 
image: {
    type : String,
},
nom: {
    type : String,
    max :255
},
prenom: {
    type : String,
    max: 1024
},
daten: {
    type : Date,
},
country: {
    type : String,
    max: 1024
},
city: {
    type : String,
    max: 1024
},
description: {
    type : String,
    max: 1024
},
sexe: {
    type : String,  
},
phone: {
    type : String,  
},
domaine: {
    type : String,  
},
note: {
    type : Number,
    enum : [0,1,2,3,4,5]
},
chat : [
    {
    idc : String,
    contactId : String,   
    nom : String,
    prenom : String,
    image : String,
    lastMessageTime : Date,
}
],
group : [
    {
    idc : String,
    contactId : String,   
    nom : String,
    image : String,
    lastMessageTime : Date,
}
]

})

module.exports =mongoose.model('Avocat',AvocatSchema);
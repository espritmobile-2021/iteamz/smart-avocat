import 'package:flutter/material.dart';
import 'package:smart_avocat/AdminDashboard.dart';
import 'package:smart_avocat/AjoutProduit.dart';
import 'package:smart_avocat/Chatbot.dart';
import 'package:smart_avocat/RdvClient.dart';
import 'package:smart_avocat/moreAvocat.dart';
import 'ListeAvocat.dart';
import 'Login.dart';
import 'Profil.dart';
import 'RdvAvocat.dart';
import 'calendrier_client.dart';
import 'more.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:async';
import 'dart:convert';
import 'package:fluttertoast/fluttertoast.dart';




class AdminNavbar extends StatefulWidget {
  @override
  _AdminNavbarState createState() => _AdminNavbarState();
}

class _AdminNavbarState extends State<AdminNavbar> {
  //var
  int _bottomIndex = 0;
  List<Widget> interfaces = [AdminDashboard(), AjoutProduit()];

  @override
  Widget build(BuildContext context) {


    return new WillPopScope(
        onWillPop: () async{
          Fluttertoast.showToast(
            msg: 'Vous êtes déjà connecté!',
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.BOTTOM,
            timeInSecForIosWeb: 1,
          );
          return false;
        },
        child:Scaffold(

          appBar: AppBar(
            title: Text("Tableau de Bord Admin"),
          ),
          drawer: Drawer(


            child: ListView(
              children: <Widget>[
                DrawerHeader(

                  child: Column(
                    children: [
                      Center(child: Text("Smart Avocat"),),
                      Image.asset("Assets/logo2.png",width: 100,),

                      Text("Bienvenue Admin")

                    ],
                  ),
                  decoration: BoxDecoration(

                    color: Colors.red[800],
                  ),
                ),



                ListTile(
                  leading: IconButton(
                    icon: Icon(Icons.power_settings_new, color: Colors.grey, size: 30,),
                  ),
                  title: Text('Déconnexion'),
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => Login()),
                    );
                  },
                ),

              ],
            ),
          ),
          bottomNavigationBar: BottomNavigationBar(
            items: [
              BottomNavigationBarItem(
                label: "Statistiques",backgroundColor: Colors.black87,
                icon: Icon(Icons.insert_chart,color: Colors.white60,),
              ),
              BottomNavigationBarItem(
                  label: "Produit",backgroundColor: Colors.black87,
                  icon: Icon(Icons.add_business_sharp,color: Colors.white60)
              ),

            ],
            currentIndex: _bottomIndex,
            onTap: (int value){
              setState(() {
                _bottomIndex = value;
              });
            },
          ),
          body: interfaces[_bottomIndex],
        ));
  }
}
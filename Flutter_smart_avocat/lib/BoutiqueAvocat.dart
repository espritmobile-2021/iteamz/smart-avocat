import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';
import 'package:smart_avocat/panier.dart';
import 'dart:convert';
import 'Login.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:io';
import 'dart:async';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';

ValueNotifier itemsNotifier = ValueNotifier([]);

class Boutique extends StatefulWidget {
  String fetchAllProducts = BaseUrl +"api/produit/getAllProducts";
  String _baseImageURL = BaseUrl+"api/avocat/file/";
  @override
  _BoutiqueState createState() => _BoutiqueState();
}
int nbProduits = 0;

class _BoutiqueState extends State<Boutique> {
  List<PostView> posts = [];
  Future<bool> fetchedProduct;
  Future<bool> fetchProduct() async {
    http.Response response = await http.get(Uri.parse(widget.fetchAllProducts));
    List<dynamic> ProductFromServer = json.decode(response.body);
    for (var item in ProductFromServer) {

      posts.add(PostView(item["_id"], item["nom"],item["quantite"],item["desc"],item["prix"],widget._baseImageURL+item["image"]));
      //PostView(this.id,this.nom,this.quantite,this.desc,this.prix,this.image);

    }
    return true;
  }

  @override
  void initState() {
    fetchedProduct=fetchProduct();
  }


  @override
  Widget build(BuildContext context) {

    return FutureBuilder(
        future: fetchedProduct,
        builder: (context,snapshot){
          if(snapshot.hasData){
            return Scaffold(
              appBar: AppBar(
                title: Text("Boutique"),
                actions: [
                  Text(nbProduits.toString()),
              IconButton(icon: Icon(Icons.shopping_cart),iconSize: 40,
              onPressed: (){
                Navigator.push(context, MaterialPageRoute(
                  builder: (BuildContext context) {
                    return Panier();
                  },
                ));
              },),

                ],
              ),
              body: GridView.builder (
                gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
                  maxCrossAxisExtent: 250,
                  childAspectRatio: 2/3,
                  crossAxisSpacing: 20,
                  mainAxisSpacing: 20
                ),
                itemBuilder: (BuildContext context, int index) {
                  return posts[index];
                },
                itemCount: posts.length,


              ),
            );
          }
          else{
            return Center(
              child: CircularProgressIndicator(),
            );
          }
        });
  }
}
List _tmpList = itemsNotifier.value;
class PostView extends StatefulWidget {


  String id,nom,quantite,desc,prix,image;
  PostView(this.id,this.nom,this.quantite,this.desc,this.prix,this.image);
  @override
  _PostViewState createState() => _PostViewState();
}
class _PostViewState extends State<PostView>{
  Map<String,dynamic> toMap(String id,String nom,String quantite,String desc,String prix,String image) {
    return {
      "id" : id,
      "nom" : nom,
      "quantite" : quantite,
      "desc" : desc,
      "prix" : prix,
      "image" : image
    };
  }

  bool etat = true;

  @override
  Widget build(BuildContext context) {
    return Material(
      child: SizedBox(height: 800,
      child: Card(
        color: Colors.white24,
        semanticContainer: true,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(15.0),
        ),
        child: Column(
          children: [
            SizedBox(height: 8,),
            ClipRRect(
                borderRadius: BorderRadius.circular(200.0),
                child: Image.network(widget.image,width: 150,),),
            SizedBox(height: 5,),
            Text(widget.nom),
            Text(widget.desc),
            Text(widget.prix + "DT"),
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [RaisedButton(
                /*
                color: (isPressed) ? Color(0xff007397)
                                  : Color(0xff9A9A9A)),*/
                color: (etat) ? Colors.red : Colors.grey,
                  child: Text("Ajouter au panier"),
                  onPressed: () async {

                    if(etat==true){
                      Database db = await openDatabase(
                          join(await getDatabasesPath(),"cart"),
                        onCreate: (Database db,int version){
                            db.execute("CREATE TABLE PANIER (id TEXT PRIMARY KEY,nom TEXT,quantite TEXT,desc TEXT,prix TEXT, image TEXT);");
                        },
                        version:1
                      );
await db.insert("PANIER", toMap(widget.id,widget.nom,widget.quantite,widget.desc,widget.prix,widget.image),conflictAlgorithm: ConflictAlgorithm.ignore);
                      setState(() {
                        nbProduits=nbProduits+1;
                        etat=false;
                      });
                    }
                    else{
                      setState(() {
                        if(nbProduits>0){
                          setState(() {
                            nbProduits=nbProduits-1;
                          });
                        }
                        etat=true;
                      });
                    }
                  }
                  )],
            )

          ],
        ),
      ),),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:smart_avocat/BoutiqueAvocat.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';

class Panier extends StatefulWidget {
  @override
  _PanierState createState() => _PanierState();
}


class _PanierState extends State<Panier> {
  List<CartView> carts = [];
  Future<bool> fetchedCart;

  Future<bool> fetchCart() async {

    Database db = await openDatabase(
      join(await getDatabasesPath(), "cart"),
    );

    List<dynamic> cartsFromDatastorage = await db.query("PANIER");

    for (var item in cartsFromDatastorage) {

      carts.add(CartView(item["id"], item["nom"], item["quantite"], item["desc"], item["prix"], item["image"]));
    }

    return true;
  }


  @override
  void initState() {
    fetchedCart=fetchCart();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Panier"),
      ),
      bottomNavigationBar: Container(
        color: Colors.black26,
        child: Row(
          children: <Widget>[
            Expanded(child: ListTile(
            title: Text("Total :"),
            subtitle: Text("230 DT"),
            ),
            ),
            Expanded(child: new MaterialButton(onPressed: () async{
              Database db = await openDatabase(
                  join(await getDatabasesPath(),"cart"),
                  onCreate: (Database db,int version){
                    db.execute("DELETE FROM PANIER");
                    db.rawDelete("DELETE FROM PANIER");

                  },
                  version:1
              );
              Navigator.push(context, MaterialPageRoute(
                builder: (BuildContext context) {
                  return Panier();
                },
              ));

            },
            child: Text("Valider",style:  TextStyle(color: Colors.white),),
            color: Colors.red,),

            )
          ],
        ),
      ),
      body: FutureBuilder(
        future: fetchedCart,
        builder: (context,snapshot){
          if(snapshot.hasData){
            return ListView.builder (
              itemBuilder: (BuildContext context, int index) {
                return carts[index];
              },
              itemCount: carts.length,
            );
          }
          
          else{return Center(
            child: CircularProgressIndicator(),
          );}
        },
      ),

    );

  }
}
class CartView extends StatelessWidget {
  String id,nom,quantite,desc,prix,image;
  CartView(this.id,this.nom,this.quantite,this.desc,this.prix,this.image);
  @override
  Widget build(BuildContext context) {
    return Material(
      child: SizedBox(height: 120,
      child:Card(
        color: Colors.white70,
          semanticContainer: true,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(15.0),
          ),
        child: Row(
          children: [
            SizedBox(width: 10,),
            ClipRRect(
              borderRadius: BorderRadius.circular(35.0),
              child: Image.network(this.image,height: 100,),),
            Column(children:[
              SizedBox(height: 10,),
              Row(
                children: [
                  SizedBox(width: 30,),
                  Column(
                    children: [
                      Text(this.nom,style: TextStyle(fontWeight: FontWeight.bold,color: Colors.black,fontSize: 20),),
                      Text(this.desc,style: TextStyle(color: Colors.black54)),
                      Divider(thickness: 5,color: Colors.red,height: 5,),
                      Text(this.prix + "  D.T.",style : TextStyle(fontSize: 20,color: Colors.red))

                    ],
                  ),
         

                ],),


            ], ),
          ],
        ),

      ),
      ),

    );
  }

}


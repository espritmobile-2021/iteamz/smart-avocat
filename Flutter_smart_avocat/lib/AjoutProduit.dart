import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:image_picker/image_picker.dart';
import 'dart:convert';
import 'dart:io';
import 'package:number_inc_dec/number_inc_dec.dart';
import 'package:smart_avocat/AdminNavbar.dart';
import 'Login.dart';


PickedFile imageURI;
final ImagePicker _picker = ImagePicker();
String state = "";
var file;
class AjoutProduit extends StatefulWidget {
  AjoutProduit();
  @override
  _AjoutProduitState createState() => _AjoutProduitState();
}


class _AjoutProduitState extends State<AjoutProduit> {
  File imageFile;
  final GlobalKey<FormState> _globalKey = new GlobalKey();
  final nomController = TextEditingController();
  final descController = TextEditingController();
  final prixController = TextEditingController();
  final quantiteController = TextEditingController();

  Future getImageFromCameraGallery() async{
    var image =  await _picker.getImage(source:  ImageSource.gallery);
    setState(() {
      imageURI = image;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Form(
        key: _globalKey,
        child: SingleChildScrollView(
          child:Column(
            children: [
              SizedBox(height: 20,),
              Center(child: Text("Ajouter produit",style: TextStyle(fontSize: 20),),),
              SizedBox(height: 50,),
              TextFormField(
                decoration: InputDecoration(
                    border: OutlineInputBorder(), labelText: "Nom"
                ),
                controller: nomController,
                // ignore: missing_return, missing_return
                validator: (value){
                  if (value.isEmpty)
                    return "Champs vide";
                },
              ),
              SizedBox(
                height: 10,
              ),
              Container(
                height: 5 * 24.0,
                child: TextFormField(
                  maxLines: 5,
                  decoration: InputDecoration(
                      border: OutlineInputBorder(), labelText: "Description"
                  ),
                  validator: (value){
                    if (value.isEmpty)
                      return "Champs Vide";
                  },
                  controller: descController,
                ),
              ),
              SizedBox(height: 10,),
              Text("Quantité:"),
              SizedBox(height: 7,),
              NumberInputWithIncrementDecrement(
                controller: quantiteController,
                min: 1,
                max: 100,
              ),
              SizedBox(height: 10),
              Text("Prix:"),
              SizedBox(height: 7,),
              NumberInputWithIncrementDecrement(
                controller: prixController,
                min: 1,
                max: 1000,
              ),
              SizedBox(height: 10,),
              SizedBox(
                height: 40,
                width: 200,
                child: FloatingActionButton.extended(
                  heroTag: "btn2",
                  elevation: 10,
                  splashColor: Colors.redAccent,

                  icon: Icon(Icons.image),
                  label: Text(
                      "Choisir une image"
                  ),

                  onPressed: () {
                    getImageFromCameraGallery();
                  },),
              ),
              SizedBox(height: 10,),
              SizedBox(
                height: 150,
                child: Container(
                  child: imageURI == null ? Image(image: AssetImage('Assets/default.jpg',),width: 50) :
                  ClipRRect(
                      borderRadius: BorderRadius.circular(200.0),
                      child:
                      Image.file(File(imageURI.path),width: 200,)),
                ),
              ),
              SizedBox(height: 10,),
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  RaisedButton(
                      child: Text("Ajouter"),
                      color: Colors.red[800],
                      onPressed: () async {
                        if(!_globalKey.currentState.validate())
                          return;

                        var res = await uploadImage(
                            nomController.text,
                            imageURI.path,
                            descController.text,
                            quantiteController.text,
                            prixController.text);
                        setState(() {
                          print(res);
                        });
                        if (res == "OK"){
                          return showDialog(context: context,builder: (context){
                            imageURI = null;
                            return CupertinoAlertDialog(
                              title: Text("Succès"),
                              content: Column(
                                  children:  [
                                    SizedBox(height: 10,),
                                    Text("Produit Créé avec succès !"),
                                    SizedBox(height: 20,),
                                    Image.asset("Assets/success.png")
                                  ]
                              ),
                              actions: [
                                CupertinoDialogAction(child: Text("OK"),
                                  onPressed: () {
                                    Navigator.push(context, MaterialPageRoute(
                                      builder: (BuildContext context) {
                                        return AdminNavbar();
                                      },
                                    ));
                                  },),

                              ],

                            );
                          });
                        }
                      }),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
Future<String> uploadImage(nom, filename, desc, quantite,prix) async {
  var url = BaseUrl+"api/produit/ajouterProduit";
  var request = http.MultipartRequest('POST', Uri.parse(url));
  request.fields['nom'] = nom;
  request.files.add(await http.MultipartFile.fromPath('file', filename));
  request.fields['desc'] = desc;
  request.fields['quantite'] = quantite;
  request.fields['prix'] = prix;
  var res = await request.send();
  return res.reasonPhrase;
}

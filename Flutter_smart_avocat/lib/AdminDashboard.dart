import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pie_chart/pie_chart.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';
import 'dart:convert';
import 'Login.dart';
import 'package:progress_dialog/progress_dialog.dart';


Future<String> fetchNom() async{
  http.Response responsedd =await http.get(Uri.parse(BaseUrl +"api/avocat/countSexe"));
  Map<String, dynamic> avocatO = json.decode(responsedd.body);
  String hom = avocatO["Homme"];
  //   print(NomCl);
  return hom;
}
Future<String> fetchFemme() async{
  http.Response responsedd =await http.get(Uri.parse(BaseUrl +"api/avocat/countSexe"));
  Map<String, dynamic> avocatO = json.decode(responsedd.body);
  String fem = avocatO["Femme"];
  //   print(NomCl);
  return fem;
}
Future<String> fetchdi() async{
  http.Response responsedd =await http.get(Uri.parse(BaseUrl +"api/avocat/countSexe"));
  Map<String, dynamic> avocatO = json.decode(responsedd.body);
  String di = avocatO["DroitImmobillier"];
  //   print(NomCl);
  return di;
}
Future<String> fetchdr() async{
  http.Response responsedd =await http.get(Uri.parse(BaseUrl +"api/avocat/countSexe"));
  Map<String, dynamic> avocatO = json.decode(responsedd.body);
  String dr = avocatO["DroitRural"];
  //   print(NomCl);
  return dr;
}
Future<String> fetchdp() async{
  http.Response responsedd =await http.get(Uri.parse(BaseUrl +"api/avocat/countSexe"));
  Map<String, dynamic> avocatO = json.decode(responsedd.body);
  String dp = avocatO["DroitPersonnes"];
  //   print(NomCl);
  return dp;
}
Future<String> fetchdpe() async{
  http.Response responsedd =await http.get(Uri.parse(BaseUrl +"api/avocat/countSexe"));
  Map<String, dynamic> avocatO = json.decode(responsedd.body);
  String dpe = avocatO["DroitPenal"];
  //   print(NomCl);
  return dpe;
}
Future<String> fetchdeco() async{
  http.Response responsedd =await http.get(Uri.parse(BaseUrl +"api/avocat/countSexe"));
  Map<String, dynamic> avocatO = json.decode(responsedd.body);
  String eco = avocatO["Droiteco"];
  //   print(NomCl);
  return eco;
}


class AdminDashboard extends StatefulWidget {
  @override
  _AdminDashboardState createState() => _AdminDashboardState();
}
Map<String,double> createMap(double homme,double femme){
  Map<String,double> mapStats = {
    "Homme" : homme,
    "Femme" : femme
  };
  return mapStats;
}
Map<String,double> createMapCat(double pers,double rur,
    double immob,double penal , double eco){
  Map<String,double> mapStats = {
    "Droit Des Personnes" : pers,
    "Droit Rural" : rur,
    "Droit Immobillier" : immob,
    "Droit Pénal" : penal,
    "Droit Economique" : eco
  };
  return mapStats;
}


class _AdminDashboardState extends State<AdminDashboard> {

  String _name= "8" ;
  String _femme= "8" ;
  String dp="3";
  String dr="3";
  String di="3";
  String dpe="3";
  String deco="3";
  void updateDP(String name) {
    setState(() {
      this.dp = name;
    });
  }
  void updateDR(String name) {
    setState(() {
      this.dr = name;
    });
  }
  void updateDI(String name) {
    setState(() {
      this.di = name;
    });
  }
  void updateDPE(String name) {
    setState(() {
      this.dpe = name;
    });
  }
  void updateDECO(String name) {
    setState(() {
      this.deco = name;
    });
  }
  void updateName(String name) {
    setState(() {
      this._name = name;
    });
  }
  void updateFemme(String name) {
    setState(() {
      this._femme = name;
    });
  }


  ////await http.get(Uri.parse(BaseUrl +"api/avocat/countSexe"));


  @override
  Widget build(BuildContext context) {
    final ProgressDialog pr = ProgressDialog(context);

    fetchNom().then((value) => updateName(value));
    fetchFemme().then((value) => updateFemme(value));
    fetchdp().then((value) => updateDP(value));
    fetchdr().then((value) => updateDR(value));
    fetchdi().then((value) => updateDI(value));
    fetchdpe().then((value) => updateDPE(value));
    fetchdeco().then((value) => updateDECO(value));

    double homme = double.parse(this._name);
    double femme = double.parse(this._femme);
    double droitPers = double.parse(this.dp);
    double droitRural = double.parse(this.dr);
    double droitImmob = double.parse(this.di);
    double droitPenal = double.parse(this.dpe);
    double droitEcon = double.parse(this.deco);

    return Container(
      child:SingleChildScrollView(
        child: Column(
          children: [
            BarProgressIndicator(
              numberOfBars: 4,
              color: Colors.white,
              fontSize: 5.0,
              barSpacing: 2.0,
              beginTweenValue: 5.0,
              endTweenValue: 13.0,
              milliseconds: 200,
            ),
            SizedBox(height: 20,),
            Text("Statistiques selon le sexe:"),
            SizedBox(height: 30,),
            
            PieChart(
            dataMap: createMap( homme, femme),
            colorList: [Colors.blueAccent,Colors.red],
            animationDuration: Duration(milliseconds: 800),
            chartLegendSpacing: 32,
            chartRadius: MediaQuery.of(context).size.width / 3.2,
            initialAngleInDegree: 0,
            chartType: ChartType.ring,
            ringStrokeWidth: 32,
            centerText: "Sexe",
            legendOptions: LegendOptions(
              showLegendsInRow: false,
              legendPosition: LegendPosition.right,
              showLegends: true,
              legendTextStyle: TextStyle(
                fontWeight: FontWeight.bold,
              ),
            ),
            chartValuesOptions: ChartValuesOptions(
              showChartValueBackground: true,
              showChartValues: true,
              showChartValuesInPercentage: false,
              showChartValuesOutside: false,
              decimalPlaces: 1,
            ),
          ),
            SizedBox(height: 30,),
            Text("Statistiques selon les catégories :"),
            SizedBox(height: 30,),
            PieChart(
              dataMap: createMapCat( droitPers, droitRural,droitImmob,droitPenal,droitEcon),
              colorList: [Colors.amberAccent,Colors.cyan,Colors.green,Colors.pink,Colors.deepPurple],
              animationDuration: Duration(milliseconds: 800),
              chartLegendSpacing: 32,
              chartRadius: MediaQuery.of(context).size.width / 3.2,
              initialAngleInDegree: 0,
              chartType: ChartType.ring,
              ringStrokeWidth: 32,
              centerText: "Sexe",
              legendOptions: LegendOptions(
                showLegendsInRow: false,
                legendPosition: LegendPosition.right,
                showLegends: true,
                legendTextStyle: TextStyle(
                  fontWeight: FontWeight.bold,
                ),
              ),
              chartValuesOptions: ChartValuesOptions(
                showChartValueBackground: true,
                showChartValues: true,
                showChartValuesInPercentage: false,
                showChartValuesOutside: false,
                decimalPlaces: 1,
              ),
            ),
          ],

        ),
      )
    );
  }


}




class BarProgressIndicator extends StatefulWidget {
  final int numberOfBars;
  final double fontSize;
  final double barSpacing;
  final Color color;
  final int milliseconds;
  final double beginTweenValue;
  final double endTweenValue;

  BarProgressIndicator({
    this.numberOfBars = 3,
    this.fontSize = 10.0,
    this.color = Colors.black,
    this.barSpacing = 0.0,
    this.milliseconds = 250,
    this.beginTweenValue = 5.0,
    this.endTweenValue = 10.0,
  });

  _BarProgressIndicatorState createState() => _BarProgressIndicatorState(
    numberOfBars: this.numberOfBars,
    fontSize: this.fontSize,
    color: this.color,
    barSpacing: this.barSpacing,
    milliseconds: this.milliseconds,
    beginTweenValue: this.beginTweenValue,
    endTweenValue: this.endTweenValue,
  );
}

class _BarProgressIndicatorState extends State<BarProgressIndicator>
    with TickerProviderStateMixin {
  int numberOfBars;
  int milliseconds;
  double fontSize;
  double barSpacing;
  Color color;
  double beginTweenValue;
  double endTweenValue;
  List<AnimationController> controllers = new List<AnimationController>();
  List<Animation<double>> animations = new List<Animation<double>>();
  List<Widget> _widgets = new List<Widget>();

  _BarProgressIndicatorState({
    this.numberOfBars,
    this.fontSize,
    this.color,
    this.barSpacing,
    this.milliseconds,
    this.beginTweenValue,
    this.endTweenValue,
  });

  initState() {
    super.initState();
    for (int i = 0; i < numberOfBars; i++) {
      _addAnimationControllers();
      _buildAnimations(i);
      _addListOfDots(i);
    }

    controllers[0].forward();
  }

  void _addAnimationControllers() {
    controllers.add(AnimationController(
        duration: Duration(milliseconds: milliseconds), vsync: this));
  }

  void _addListOfDots(int index) {
    _widgets.add(Padding(
      padding: EdgeInsets.only(right: barSpacing),
      child: _AnimatingBar(
        animation: animations[index],
        fontSize: fontSize,
        color: color,
      ),
    ));
  }

  void _buildAnimations(int index) {
    animations.add(
        Tween(begin: widget.beginTweenValue, end: widget.endTweenValue)
            .animate(controllers[index])
          ..addStatusListener((AnimationStatus status) {
            if (status == AnimationStatus.completed)
              controllers[index].reverse();
            if (index == numberOfBars - 1 &&
                status == AnimationStatus.dismissed) {
              controllers[0].forward();
            }
            if (animations[index].value > widget.endTweenValue / 2 &&
                index < numberOfBars - 1) {
              controllers[index + 1].forward();
            }
          }));
  }

  Widget build(BuildContext context) {
    return SizedBox(
      height: 30.0,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.end,
        mainAxisAlignment: MainAxisAlignment.center,
        children: _widgets,
      ),
    );
  }

  dispose() {
    for (int i = 0; i < numberOfBars; i++) controllers[i].dispose();
    super.dispose();
  }
}

class _AnimatingBar extends AnimatedWidget {
  final Color color;
  final double fontSize;
  _AnimatingBar(
      {Key key, Animation<double> animation, this.color, this.fontSize})
      : super(key: key, listenable: animation);

  Widget build(BuildContext context) {
    final Animation<double> animation = listenable;
    return Container(
      height: animation.value,
      decoration: BoxDecoration(
        shape: BoxShape.rectangle,
        border: Border.all(color: color),
        borderRadius: BorderRadius.circular(2.0),
        color: color,
      ),
      width: fontSize,
    );
  }
}





